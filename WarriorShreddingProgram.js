import React from 'react';
import { Button, View } from 'react-native';

class WarriorShreddingProgram extends React.Component {

    constructor(props){
      super(props);
      this.state = {
        
      }
    }
  
    static navigationOptions = {
      title: 'Warrior Shredding',
    };
    render() {

      const {navigate} = this.props.navigation; 
      return (
        <View>
          <Button
            onPress={() => navigate('ShoulderAndBackRoutine', {
                programDuration: this.props.navigation.getParam('programDuration'),
              }
            )}
            title="Shoulder and Back Routine"
            />
        </View>);
    }
  }

  
  export default WarriorShreddingProgram;