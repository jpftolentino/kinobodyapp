import React from 'react';
import { Button, View, TextInput, Text } from 'react-native';

class SetWorkout extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.getParam('data', 'Title is undefined'),
    };
    this.setWeight = this.setWeight.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('workoutTitle', 'Title is undefined'),
    };
  };

  setWeight(weight, id){
    this.state.data.workoutData.sets[id-1].weight = weight;
  }

  render() {

    const sets = this.state.data.workoutData.sets;
    const setTemplate = [];
    
    for(var i = 0; i < sets.length; i++){
      const setid = sets[i].id;
      const textid = "text"+setid;
      const viewid = "view"+setid;
      const textinputid = "textinputid"+setid;
      setTemplate.push(
        <View key={viewid}>
          <Text key={textid}>Set {setid}</Text>
          <TextInput
              placeholder="lbs"
              onChangeText={(weight) => this.setWeight(weight, setid)}
              key={textinputid}
          />
        </View>
      );
    }

    return (
      <View>
        {setTemplate}
        <Button
          onPress={() => 
            this.props.navigation.navigate('DoWorkout', {  
              programDuration: this.props.navigation.getParam('programDuration','undefined'),
              workoutTitle: this.state.workoutTitle,
              data: this.state.data.workoutData,
              numberOfSets: this.state.data.workoutData.numberOfSets,
            })
          }
            title="Start"
          />
      </View>); 
  }
}

export default SetWorkout;