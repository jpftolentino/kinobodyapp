import React from 'react';
import { View } from 'react-native';
import Workout from './Workout.js'

class ShoulderAndBackRoutine extends React.Component {

  /*
  Shoulder and Back 

  1. Weighted Chin-ups: 2 sets - 4-6 reps, 6-8 reps (RPT)
  2. Standing Barbell Press: 2 sets - 4-6 reps, 6-8 reps (RPT)
  3. Seated Cable Rows: 2 sets - 4-6 reps, 6-8 reps (RPT)
  4. Lateral Raises: 4 sets x 10-15 reps (SS)
  5. Hanging Legs Raises: 2 sets x 10-15 reps (SS)
  */

  constructor(props){
    super(props);
    this.state = {
      data: {
        
        lateralRaises: 
        {
          id: 1,
          programId: 'WarriorShreddingProgram',
          routineId: 'ShoulderAndBackRoutine',
          name: 'Lateral Raises (SS)',
          week: 0,
          finished: false,
          numberOfSets: 4,
          sets: [
            {
              id: 1,
              recommendedReps: [10,15],
              weight: 0,
              reps: 0,
              date: "",
            },
            {
              id: 2,
              recommendedReps: [10,15],
              weight: 0,
              reps: 0,
              date: "",
            },
            {
              id: 3,
              recommendedReps: [10,15],
              weight: 0,
              reps: 0,
              date: "",
            },
            {
              id: 4,
              recommendedReps: [10,15],
              weight: 0,
              reps: 0,
              date: "",
            },
          ]
        },

        hangingLegRaises: 
        {
          id: 2,
          programId: 'WarriorShreddingProgram',
          routineId: 'ShoulderAndBackRoutine',
          name: 'Hanging Leg Raises (SS)',
          week: 0,
          finished: false,
          numberOfSets: 2,
          sets: [
            {
              id: 1,
              recommendedReps: [10,15],
              weight: 0,
              reps: 0,
              date: "",
            },
            {
              id: 2,
              recommendedReps: [10,15],
              weight: 0,
              reps: 0,
              date: "",
            }
          ]
        },

        weightedChinUps: 
        {
          id: 3,
          programId: 'WarriorShreddingProgram',
          routineId: 'ShoulderAndBackRoutine',
          name: 'Weighted Chin-ups (RPT)',
          week: 0,
          finished: false,
          numberOfSets: 2,
          sets: [
            {
              id: 1,
              recommendedReps: [4,6],
              weight: 0,
              reps: 0,
              date: "",
            },
            {
              id: 2,
              recommendedReps: [6,8],
              weight: 0,
              reps: 0,
              date: "",
            }
          ]
        },

        standingBarbellPress: 
        {
          id: 4,
          programId: 'WarriorShreddingProgram',
          routineId: 'ShoulderAndBackRoutine',
          name: 'Standing Barbell Press (RPT)',
          week: 0,
          finished: false,
          numberOfSets: 2,
          sets: [
            {
              id: 1,
              recommendedReps: [4,6],
              weight: 0,
              reps: 0,
              date: "",
            },
            {
              id: 2,
              recommendedReps: [6,8],
              weight: 0,
              reps: 0,
              date: "",
            }
          ]
        },
        
        seatedCableRows: 
        {
          id: 5,
          programId: 'WarriorShreddingProgram',
          routineId: 'ShoulderAndBackRoutine',
          name: 'Seated Cable Rows (RPT)',
          week: 0,
          finished: false,
          numberOfSets: 2,
          sets: [
            {
              id: 1,
              recommendedReps: [4,6],
              weight: 0,
              reps: 0,
              date: "",
            },
            {
              id: 2,
              recommendedReps: [6,8],
              weight: 0,
              reps: 0,
              date: "",
            }
          ]
        }

      }
    };
  }
  
  static navigationOptions = {
    title: 'Shoulder and Back',
  };

  //loud routine data and set new data state, if object doesn't exist in database use default routine

  render() {
    
    return (
      <View>
        <Workout 
        navigation={this.props.navigation} 
        workoutData={this.state.data.weightedChinUps}
        />
      </View>);
  }
}

export default ShoulderAndBackRoutine;