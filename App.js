import React from 'react';
import { Button, View, Text, AsyncStorage } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import WarriorShreddingProgram from './WarriorShreddingProgram.js';
import ShoulderAndBackRoutine from './ShoulderAndBackRoutine.js';
import Workout from './Workout.js';
import SetWorkout from './SetWorkout.js';
import DoWorkout from './DoWorkout.js';


class KinobodyPrograms extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      programDuration: {
        warriorShredding:{
          week: 0,
          duration: 0,
          date: 0,
        },
        agressiveFatLoss:{
          week: 0,
          duration: 0,
          date: 0,
        },
        superHero:{
          week: 0,
          duration: 0,
          date: 0,
        },
        greekGod:{
          week: 0,
          duration: 0,
          date: 0,
        }
      }
    }
  }

  static navigationOptions = {
    title: 'Kinobody Assistant',
  };

  displayWeek = async () => {
    try {
      let programDuration = await AsyncStorage.getItem('programDuration');
      if(programDuration !== null){
        this.setState({programDuration});
      } else {
        let programDuration = Object.assign({}, this.state.programDuration);
        programDuration.warriorShredding.week = 'Start';
        programDuration.warriorShredding.duration = '7 days';
        programDuration.agressiveFatLoss.week = 'Start';
        programDuration.agressiveFatLoss.duration = '7 days';
        programDuration.superHero.week = 'Start';
        programDuration.superHero.duration = '7 days';
        programDuration.greekGod.week = 'Start';
        programDuration.greekGod.duration = '7 days';
        this.setState({programDuration});
      }
    }

    catch(error) {
      alert(error);
    }
  }

  //It suppose to run the function once it loads to get the most updated week data but it's not being called.
  componentDidMount(){
    {this.displayWeek()}
  }

  render() {
    
    const programDuration = this.state.programDuration;

    return (
      <View>
        <Text>Warrior Shredding Program</Text>
        <Text>Week {programDuration.warriorShredding.week} - {programDuration.warriorShredding.duration} left</Text>
        <Button
            onPress={() => this.props.navigation.navigate('WarriorShreddingProgram', {
                programDuration: this.state.programDuration,
              })
            }
            title="Go"
          />
      </View>);
  }
}

const SimpleApp = createStackNavigator({
  Home: { screen: KinobodyPrograms },

  WarriorShreddingProgram: { screen: WarriorShreddingProgram},

  ShoulderAndBackRoutine: { screen: ShoulderAndBackRoutine },

  Workout: { screen: Workout },

  SetWorkout: { screen: SetWorkout },

  DoWorkout: { screen: DoWorkout }

});

export default SimpleApp;
