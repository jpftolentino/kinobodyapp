import React from 'react';
import { Button, View, Text } from 'react-native';

class Workout extends React.Component {

  constructor(props){
    super(props);
    this.state = {

    }
  }

  render() {

    const workoutData = this.props.workoutData;
    const name = this.props.workoutData.name;
    const numberOfSets = this.props.workoutData.numberOfSets;
    let recommendedReps = '';

    for(var i = 0; i < this.props.workoutData.sets.length; i++){
      for(var j = 0; j < this.props.workoutData.sets[i].recommendedReps.length; j++){
        recommendedReps += this.props.workoutData.sets[i].recommendedReps[j];
        if(j == 0){
          recommendedReps += "-";
        }
      }
      recommendedReps += ' Reps, ';
    }

    recommendedReps = recommendedReps.slice(0, -2);

    return (
      <View>
        <Text>{name}</Text>
        <Text>{numberOfSets} Sets</Text>
        <Text>{recommendedReps}</Text>
        <Button
          onPress={() => this.props.navigation.navigate('SetWorkout', {
              programDuration: this.props.navigation.getParam('programDuration','UndefinedValue'),
              workoutTitle: this.props.workoutData.name,
              data: {workoutData},
          })
        }
          title="Start"
          />
      </View>);
  }
}

export default Workout;