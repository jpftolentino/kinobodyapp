import React from 'react';
import { Button, View, Text, Image } from 'react-native';

class DoWorkout extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.getParam('data', 'state data does not contain an object'),
      setCount: this.props.navigation.getParam('numberOfSets', 'state setCount does is undefined'),
      repCount : 0,
      currentSet: 1,
    }
    this.handleDec = this.handleDec.bind(this);
    this.handleInc = this.handleInc.bind(this);
    this.finishSet = this.finishSet.bind(this);
  }
  
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('workoutTitle', 'Title is undefined'),
    };
  };

    handleDec() {
      this.setState({
        repCount: this.state.repCount - 1
      })
    }

    handleInc() {
      this.setState({
        repCount: this.state.repCount + 1
      })
    }

    finishSet() {
      const date = new Date();
      this.state.data.sets[this.state.currentSet-1].date = date;
      this.state.data.sets[this.state.currentSet-1].reps = this.state.repCount;

      if(this.state.setCount != 1){ 
        console.log(this.state.data);
        this.setState({
          setCount: this.state.setCount - 1,
          currentSet: this.state.currentSet + 1,
          repCount: 0,
        })
      } else {
        //Save the routine data
        console.log('SAVE DATA AND NAVIGATE BACK TO ShoulderAndBackRoutine.js');
        this.state.data.finished = true;
        console.log(this.state.data);
        console.log(this.state.data.routineId);
        this.props.navigation.navigate(this.state.data.routineId, {
          data: this.state.data,
        });
        // AsyncStorage.setItem('warriorShreddingProgramDuration',)
      }
    }

    render() {
        // Show current weight of set
        // If rep below min yellow - Push it!
        // If rep within range green - Good job!
        // If rep more than range red - Nice work, increase your weight! 

        console.log(this.props.navigation.getParam('programDuration','Undefined Value'));

        return (
          <View>
            <Text>Set {this.state.currentSet},{this.state.setCount}, {this.state.data.numberOfSets}</Text>
            <Text>Weight: {this.state.data.sets[this.state.currentSet-1].weight}</Text>
            <Text>Reps: {this.state.repCount}</Text>
            <Button 
              onPress={this.handleDec}
              title="-"
            />
            <Button 
              onPress={this.handleInc}
              title="+"
            />
            <Button
              onPress={this.finishSet}
              title="Finish"
            />
          </View> ); 
      
    }
  }

  
  export default DoWorkout;